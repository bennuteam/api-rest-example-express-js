CREATE DATABASE book_db;

CREATE TABLE book_db.book (
	isbn varchar(255) NOT NULL,
	title varchar(255),
	author varchar(255),
	publish_date date,
	publisher varchar(255),
	numOfPages int,
    PRIMARY KEY (isbn)
);