const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const db =  require('mysql');
const app = express();
const port = 3000;
const conn = db.createConnection({
    host: '172.17.0.2',
    user: 'root',
    password: 'my-secret-pw',
    database: 'book_db'
});
conn.connect();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/book', (req, res) => {
    const book = req.body;
    let values = [[book.isbn, book.title, book.author, book.publish_date, book.publisher, book.numOfPages]];
    const sql = 'INSERT INTO book (isbn, title, author, publish_date, publisher, numOfPages) VALUES ?'
    conn.query(sql, [values], function(err, result) {
        if (err) {
            throw err;
        }
        console.log('Records inserted: ', result.affectedRows);
        res.send('{"msg":"ok"}');
    });
});

app.get('/books', (req, res) => {
    const sql = 'SELECT * FROM book'
    let books;
    conn.query(sql, function(err, result) {
        if (err) {
            throw err;
        }
        books = result.map(v => Object.assign({}, v));
        res.send(books);
    });
});

app.put('/book/:isbn', (req, res) => {
    const isbn = req.params.isbn;
    const book = req.body;
    const sql = 'UPDATE book SET title=?, author=?, publish_date=?, publisher=?, numOfPages=? WHERE isbn=?';
    let values = [book.title, book.author, book.publish_date, book.publisher, book.numOfPages, book.isbn];
    conn.query(sql, values, function(err, result) {
        if (err) {
            throw err;
        }
        res.send('{"msg":"ok"}');
    });
});

app.get('/book/:isbn', (req, res) => {
    const isbn = req.params.isbn;
    const sql = 'SELECT * FROM book WHERE isbn=?';
    let book;
    conn.query(sql,[isbn], function(err, result) {
        if (err) {
            throw err;
        }
        book = result.map(v => Object.assign({}, v));
        res.send(book);
    });

});

app.delete('/book/:isbn', (req, res) => {
    const isbn = req.params.isbn;
    const sql = 'DELETE FROM book WHERE isbn=?';
    conn.query(sql,[isbn],function(err, result) {
        if (err) {
            throw err;
        }
        res.send('{"msg":"ok"}');
    });

});

app.listen(port, () => console.log(`app listening on port ${port}!`));
